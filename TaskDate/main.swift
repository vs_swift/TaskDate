//
//  main.swift
//  TaskDate
//
//  Created by Private on 12/19/17.
//  Copyright © 2017 Private. All rights reserved.
//

import Foundation

var people: [Human] = []

for _ in 1...10 {
    let year = String(2002 - arc4random_uniform(34))
    let day = String(1 + arc4random_uniform(31))
    let month = String(1 + arc4random_uniform(12))
    
    let myDate = day + "/" + month + "/" + year
    let data = DateFormatter()
    data.dateFormat = "dd/MM/yyyy"
    let born = data.date(from: myDate)
    
    if let verification = born{
        let human = Human(dateOfBirth: verification)
        people.append(human)
    } else {
        print("Not verified")
    }
}


func showDayOfBirth(date: Date) -> String {
    let input = DateFormatter()
    input.dateFormat = "dd/MM/yyyy"
    return input.string(from:date)
}

for i in people {
    let someDate = showDayOfBirth(date: i.dateOfBirth)
    print(someDate)
}



for i in people.sorted(by: {$0.dateOfBirth > $1.dateOfBirth}) {
    let someDate = showDayOfBirth(date: i.dateOfBirth)
}

let firstName = ["Roman", "Kelly", "Zach", "Julia", "Oliver"]
let lastName =  ["Melvin","Hughes","Adams","Stark","Brown","Tye","Choi","Davis","Mitchell","Dunnett"]
var fullName = [String]()

for i in people.sorted(by: {$0.dateOfBirth > $1.dateOfBirth}) {
    let randomNames = Int(arc4random_uniform(UInt32(firstName.count)))
    let randomSurname = Int(arc4random_uniform(UInt32(lastName.count)))

    let addFirstName = "\(firstName[randomNames])"
    let addLastName: String = "\(lastName[randomSurname])"
    
    let newformat = DateFormatter()
    newformat.dateFormat = "YYYY"
    let v = newformat.string(from: i.dateOfBirth)
    fullName += [addFirstName + " " + addLastName + " " + v]
}

for value in fullName {
    print(value)
}


var new = [Date]()
for i in people.sorted(by: {$0.dateOfBirth > $1.dateOfBirth}) {
    new.append(i.dateOfBirth)
}

let start = new[0]
let last = new.count - 1
let end = new[last]

let format = DateComponentsFormatter()
var brand = format.string(from: end, to: start)
var newString = brand?.components(separatedBy: " ")


for (value,item) in newString!.enumerated() {
    let last = newString!.count - 1
    if value < last {
        print("\(item)", terminator: " ")
    }
}



let date = Date()

let components = DateComponents(year: 2018)
let calendar = Calendar.current
let dates = calendar.date(from: components)!

let range = calendar.range(of: .month, in: .year, for: date)!
let numDays = range.count

var strr = [Any]()
for i in 1...numDays {
    var components = Calendar.current.dateComponents([.year, .month], from: date)
    components.month = i
    components.day = 1
    let data = Calendar.current.date(from: components)
    strr.append((data) as Any)
}

func dayName(stringDate: String) -> String {
    let format  = DateFormatter()
    format.dateFormat = "dd/MM/yyyy"
    let date = format.date(from: stringDate)!
    format.dateFormat = "EEEE"
    return format.string(from: date)
}

var final = [String]()
for i in strr {
    let str = showDayOfBirth(date: i as! Date)
    final.append(str)
}

for i in final {
    print(dayName(stringDate: i))
}



func sundays (month : Int ) {

    var dateComponents = DateComponents()
    dateComponents.year = 2018
    dateComponents.month = month

    let calendar = Calendar.current
    let date = calendar.date(from: dateComponents)!

    let range = calendar.range(of: .day, in: .month, for: date)
    let daysOfMonth = range!.count

    let dateFormatter = DateFormatter()
    for i in 1...daysOfMonth {
        dateComponents.day = i

        let date = calendar.date(from: dateComponents)!
        dateFormatter.dateFormat = "EEEE"
        let dayOfWeek = dateFormatter.string(from: date)

        if dayOfWeek == "Sunday" {
            dateFormatter.dateFormat = "dd/MM"
            let sundays = dateFormatter.string(from: date)
            print(sundays)
        }
    }

}

for i in 1...12 {
    sundays(month: i)
}



func workingDays (month : Int ) -> Int {

    var dateComponents = DateComponents()
    dateComponents.year = 2018
    dateComponents.month = month

    let calendar = Calendar.current
    let date = calendar.date(from: dateComponents)!

    let range = calendar.range(of: .day, in: .month, for: date)
    var daysOfMonth = range!.count

    var numberOfWorkingDays = 0

    let dateFormatter = DateFormatter()
    for i in 1...daysOfMonth {
        dateComponents.day = i

        let date = calendar.date(from: dateComponents)!
        dateFormatter.dateFormat = "EEEE"
        let dayOfWeek = dateFormatter.string(from: date)

        if dayOfWeek == "Saturday" {
            daysOfMonth -= 1
        }
        if dayOfWeek == "Sunday" {
            daysOfMonth -= 1
        }
        numberOfWorkingDays = daysOfMonth
    }
    return numberOfWorkingDays
}

for i in 1...12 {
    print(workingDays(month: i))
}

