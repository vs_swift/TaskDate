//
//  Human.swift
//  TaskDate
//
//  Created by Private on 12/19/17.
//  Copyright © 2017 Private. All rights reserved.
//

import Foundation

class Human {
    var dateOfBirth: Date
    
    init(dateOfBirth: Date) {
        self.dateOfBirth = dateOfBirth
    }
}
